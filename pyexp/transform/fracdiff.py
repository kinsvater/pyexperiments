import numpy as np
import pandas as pd


def get_ffd_weights(d, tol=1e-2):
    '''
    Copied from "Advances in Financial Machine Learning".
    Fractional differencing is same as weighted averaging of a series.
    This function gives you all (significant) weights w for given
    difference parameter d with abs(w)>=tol. This guarantees that
    the series of weights is finite, at the cost of a small error.
    Input:
        float d: non-zero, differencing paramter
        float tol: non-zero, only significant weights with w>=tol included into computation
    Output:
        np.array of floats: vector of weights with shape (n,1) where n
            is the number of significant weights.
    '''
    # tol > 0 drops insignificant weights
    weights, k = [1.], 1
    while True:
        weight_ = -weights[-1] / k*(d-k+1)
        if abs(weight_) < tol: break
        weights.append(weight_)
        k += 1
    weights = np.array(weights[::-1]).reshape(-1,1)
    return weights


def fracdiff(self, d, tol=1e-2):
    '''
    Fractional differencing for pd.Series and pd.DataFrame. Use only
    by extending those classes through:
    > setattr(pd.Series, 'fracdiff', fracdiff)
    Input:
        float d: non-zero, differencing paramter.
        float tol: non-zero, only significant weights with w>=tol included into computation if tol>0.
    '''
    weights = get_ffd_weights(d, tol)
    return self.rolling(len(weights)).apply(lambda x: np.dot(weights.T, x), raw=False)


if __name__ == '__main__':
    # how to compute fractional differnces of a series:
    d = 0.7
    weights = get_ffd_weights(d)
    
    # example series:
    series = pd.Series(np.random.randn(500))
    
    # fractional differnces:
    print(series.rolling(len(weights)).apply(lambda x: np.dot(weights.T, x)))#, raw=True))
    
    # Dynamically add fracdiff method to pandas:
    setattr(pd.Series, 'fracdiff', fracdiff)
    # Use this to accomplish same for data frames:
    #setattr(pd.DataFrame, 'fracdiff', fracdiff)
    
    print(series.fracdiff(d=0.7))
    
    