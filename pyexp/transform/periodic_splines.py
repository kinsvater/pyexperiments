import numpy as np
import pandas as pd
from patsy import dmatrix


def add_periodic_splines(df, period_column, degrees_of_freedom=4, remove_period_column=True):
    """
    Add periodic spline scores to the dataframe df as additional regressors.
    ---
    Expects as input the dataframe df, with column calendar_column representing the periodicity in numbers between 0 and 1.
    E.g., if your observations are on weekly frequency and you want to model yearly periodicity, you should add first
    column df[calendar_column] = df[date].dt.week / 53. Here the column df[date] is of type datetime.
    
    Input:
        df: pd.DataFrame of observations which should be extended by periodic spline regressor columns.
        calendar_column: name of the column representing the periodicity as numbers between 0 and 1.
        degrees_of_freedom: integer setting the complexity of the spline fit.
        remove_calendar_column: bool; whether the calendar_column should be removed before return of the extended df.
    Output:
        pd.DataFrame extended by periodic spline scores columns ready to be used in a linear model.
    """
    # the 'cc' part in the string of the dmatrix call is responsible for periodic splines.
    scores_matrix = dmatrix("cc("+period_column+", df={}, constraints='center') - 1".format(degrees_of_freedom),
                              data=df, return_type='dataframe')
    
    # rename columns for convenience
    cols = scores_matrix.columns
    scores_matrix = scores_matrix.rename(columns={col: 'spline_score_{}'.format(num+1) for num, col in enumerate(cols)})
    df[scores_matrix.columns] = scores_matrix
    
    if remove_period_column:
        df = df.drop(period_column, axis=1)
    
    return df